﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeeWorld
{
    public class BeeConfig
    {
        public int InitialBees;
        public int MaximumBees;
        public double InitialHoney;
        public double MaximumHoney;

        public BeeConfig(int iniBees, int maxBees, double iniHoney, double maxHoney)
        {
            this.InitialBees = iniBees;
            this.MaximumBees = maxBees;
            this.InitialHoney = iniHoney;
            this.MaximumHoney = maxHoney;
        }
    }


}
