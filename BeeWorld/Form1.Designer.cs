﻿namespace BeeWorld
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.ToolStrip1 = new MyToolStrip();
            this.StartBtn = new System.Windows.Forms.ToolStripButton();
            this.StopBtn = new System.Windows.Forms.ToolStripButton();
            this.SettingsBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.OpenToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.NameToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.printToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.BeesLbl2 = new System.Windows.Forms.Label();
            this.BeesLbl1 = new System.Windows.Forms.Label();
            this.FlowersLbl2 = new System.Windows.Forms.Label();
            this.FlowersLbl1 = new System.Windows.Forms.Label();
            this.HoneyLbl1 = new System.Windows.Forms.Label();
            this.HoneyLbl2 = new System.Windows.Forms.Label();
            this.NectarLbl1 = new System.Windows.Forms.Label();
            this.NectarLbl2 = new System.Windows.Forms.Label();
            this.FramesRunLbl1 = new System.Windows.Forms.Label();
            this.FramesRunLbl2 = new System.Windows.Forms.Label();
            this.FrameRateLbl1 = new System.Windows.Forms.Label();
            this.FrameRateLbl2 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.listBox = new System.Windows.Forms.ListBox();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.ToolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ToolStrip1
            // 
            this.ToolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StartBtn,
            this.StopBtn,
            this.SettingsBtn,
            this.toolStripSeparator1,
            this.OpenToolStripButton,
            this.NameToolStripButton,
            this.printToolStripButton});
            this.ToolStrip1.Location = new System.Drawing.Point(0, 0);
            this.ToolStrip1.Name = "ToolStrip1";
            this.ToolStrip1.Size = new System.Drawing.Size(325, 25);
            this.ToolStrip1.TabIndex = 0;
            this.ToolStrip1.Text = "toolStrip1";
            // 
            // StartBtn
            // 
            this.StartBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.StartBtn.Image = ((System.Drawing.Image)(resources.GetObject("StartBtn.Image")));
            this.StartBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.StartBtn.Name = "StartBtn";
            this.StartBtn.Size = new System.Drawing.Size(95, 22);
            this.StartBtn.Text = "Start Simulation";
            this.StartBtn.Click += new System.EventHandler(this.StartBtn_Click);
            // 
            // StopBtn
            // 
            this.StopBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.StopBtn.Image = ((System.Drawing.Image)(resources.GetObject("StopBtn.Image")));
            this.StopBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.StopBtn.Name = "StopBtn";
            this.StopBtn.Size = new System.Drawing.Size(35, 22);
            this.StopBtn.Text = "Stop";
            this.StopBtn.Click += new System.EventHandler(this.StopBtn_Click);
            // 
            // SettingsBtn
            // 
            this.SettingsBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.SettingsBtn.Image = ((System.Drawing.Image)(resources.GetObject("SettingsBtn.Image")));
            this.SettingsBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SettingsBtn.Name = "SettingsBtn";
            this.SettingsBtn.Size = new System.Drawing.Size(53, 22);
            this.SettingsBtn.Text = "Settings";
            this.SettingsBtn.Click += new System.EventHandler(this.SettingsBtn_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // OpenToolStripButton
            // 
            this.OpenToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.OpenToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("OpenToolStripButton.Image")));
            this.OpenToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OpenToolStripButton.Name = "OpenToolStripButton";
            this.OpenToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.OpenToolStripButton.Text = "&Открыть";
            this.OpenToolStripButton.Click += new System.EventHandler(this.OpenToolStripButton_Click);
            // 
            // NameToolStripButton
            // 
            this.NameToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.NameToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("NameToolStripButton.Image")));
            this.NameToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.NameToolStripButton.Name = "NameToolStripButton";
            this.NameToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.NameToolStripButton.Text = "&Сохранить";
            this.NameToolStripButton.Click += new System.EventHandler(this.SaveToolStripButton_Click);
            // 
            // printToolStripButton
            // 
            this.printToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.printToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("printToolStripButton.Image")));
            this.printToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printToolStripButton.Name = "printToolStripButton";
            this.printToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.printToolStripButton.Text = "Print";
            this.printToolStripButton.ToolTipText = "Печать";
            this.printToolStripButton.Click += new System.EventHandler(this.printToolStripButton_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 290);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(325, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // StatusLabel
            // 
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(45, 17);
            this.StatusLabel.Text = "Paused";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.BeesLbl2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.BeesLbl1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.FlowersLbl2, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.FlowersLbl1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.HoneyLbl1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.HoneyLbl2, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.NectarLbl1, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.NectarLbl2, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.FramesRunLbl1, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.FramesRunLbl2, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.FrameRateLbl1, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.FrameRateLbl2, 1, 5);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 25);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(325, 119);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // BeesLbl2
            // 
            this.BeesLbl2.AutoSize = true;
            this.BeesLbl2.Location = new System.Drawing.Point(165, 0);
            this.BeesLbl2.Name = "BeesLbl2";
            this.BeesLbl2.Size = new System.Drawing.Size(31, 13);
            this.BeesLbl2.TabIndex = 0;
            this.BeesLbl2.Text = "Bees";
            // 
            // BeesLbl1
            // 
            this.BeesLbl1.AutoSize = true;
            this.BeesLbl1.Location = new System.Drawing.Point(3, 0);
            this.BeesLbl1.Name = "BeesLbl1";
            this.BeesLbl1.Size = new System.Drawing.Size(38, 13);
            this.BeesLbl1.TabIndex = 2;
            this.BeesLbl1.Text = "#Bees";
            // 
            // FlowersLbl2
            // 
            this.FlowersLbl2.AutoSize = true;
            this.FlowersLbl2.Location = new System.Drawing.Point(165, 19);
            this.FlowersLbl2.Name = "FlowersLbl2";
            this.FlowersLbl2.Size = new System.Drawing.Size(43, 13);
            this.FlowersLbl2.TabIndex = 1;
            this.FlowersLbl2.Text = "Flowers";
            // 
            // FlowersLbl1
            // 
            this.FlowersLbl1.AutoSize = true;
            this.FlowersLbl1.Location = new System.Drawing.Point(3, 19);
            this.FlowersLbl1.Name = "FlowersLbl1";
            this.FlowersLbl1.Size = new System.Drawing.Size(50, 13);
            this.FlowersLbl1.TabIndex = 3;
            this.FlowersLbl1.Text = "#Flowers";
            // 
            // HoneyLbl1
            // 
            this.HoneyLbl1.AutoSize = true;
            this.HoneyLbl1.Location = new System.Drawing.Point(3, 38);
            this.HoneyLbl1.Name = "HoneyLbl1";
            this.HoneyLbl1.Size = new System.Drawing.Size(115, 13);
            this.HoneyLbl1.TabIndex = 4;
            this.HoneyLbl1.Text = "Total honey in the hive";
            // 
            // HoneyLbl2
            // 
            this.HoneyLbl2.AutoSize = true;
            this.HoneyLbl2.Location = new System.Drawing.Point(165, 38);
            this.HoneyLbl2.Name = "HoneyLbl2";
            this.HoneyLbl2.Size = new System.Drawing.Size(69, 13);
            this.HoneyLbl2.TabIndex = 5;
            this.HoneyLbl2.Text = "HoneyInHive";
            // 
            // NectarLbl1
            // 
            this.NectarLbl1.AutoSize = true;
            this.NectarLbl1.Location = new System.Drawing.Point(3, 58);
            this.NectarLbl1.Name = "NectarLbl1";
            this.NectarLbl1.Size = new System.Drawing.Size(115, 13);
            this.NectarLbl1.TabIndex = 6;
            this.NectarLbl1.Text = "Total nectar in the field";
            // 
            // NectarLbl2
            // 
            this.NectarLbl2.AutoSize = true;
            this.NectarLbl2.Location = new System.Drawing.Point(165, 58);
            this.NectarLbl2.Name = "NectarLbl2";
            this.NectarLbl2.Size = new System.Drawing.Size(84, 13);
            this.NectarLbl2.TabIndex = 7;
            this.NectarLbl2.Text = "NectarInFlowers";
            // 
            // FramesRunLbl1
            // 
            this.FramesRunLbl1.AutoSize = true;
            this.FramesRunLbl1.Location = new System.Drawing.Point(3, 78);
            this.FramesRunLbl1.Name = "FramesRunLbl1";
            this.FramesRunLbl1.Size = new System.Drawing.Size(59, 13);
            this.FramesRunLbl1.TabIndex = 8;
            this.FramesRunLbl1.Text = "Frames run";
            // 
            // FramesRunLbl2
            // 
            this.FramesRunLbl2.AutoSize = true;
            this.FramesRunLbl2.Location = new System.Drawing.Point(165, 78);
            this.FramesRunLbl2.Name = "FramesRunLbl2";
            this.FramesRunLbl2.Size = new System.Drawing.Size(61, 13);
            this.FramesRunLbl2.TabIndex = 9;
            this.FramesRunLbl2.Text = "FramesRun";
            // 
            // FrameRateLbl1
            // 
            this.FrameRateLbl1.AutoSize = true;
            this.FrameRateLbl1.Location = new System.Drawing.Point(3, 98);
            this.FrameRateLbl1.Name = "FrameRateLbl1";
            this.FrameRateLbl1.Size = new System.Drawing.Size(57, 13);
            this.FrameRateLbl1.TabIndex = 10;
            this.FrameRateLbl1.Text = "Frame rate";
            // 
            // FrameRateLbl2
            // 
            this.FrameRateLbl2.AutoSize = true;
            this.FrameRateLbl2.Location = new System.Drawing.Point(165, 98);
            this.FrameRateLbl2.Name = "FrameRateLbl2";
            this.FrameRateLbl2.Size = new System.Drawing.Size(59, 13);
            this.FrameRateLbl2.TabIndex = 11;
            this.FrameRateLbl2.Text = "FrameRate";
            // 
            // listBox
            // 
            this.listBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.listBox.FormattingEnabled = true;
            this.listBox.Location = new System.Drawing.Point(0, 143);
            this.listBox.Name = "listBox";
            this.listBox.Size = new System.Drawing.Size(325, 147);
            this.listBox.TabIndex = 3;
            // 
            // timer2
            // 
            this.timer2.Interval = 150;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(325, 312);
            this.Controls.Add(this.listBox);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.ToolStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "BeeHive Simulator";
            this.Move += new System.EventHandler(this.Form1_Move);
            this.ToolStrip1.ResumeLayout(false);
            this.ToolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MyToolStrip ToolStrip1;
        private System.Windows.Forms.ToolStripButton StartBtn;
        private System.Windows.Forms.ToolStripButton StopBtn;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel StatusLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label BeesLbl2;
        private System.Windows.Forms.Label BeesLbl1;
        private System.Windows.Forms.Label FlowersLbl2;
        private System.Windows.Forms.Label FlowersLbl1;
        private System.Windows.Forms.Label HoneyLbl1;
        private System.Windows.Forms.Label HoneyLbl2;
        private System.Windows.Forms.Label NectarLbl1;
        private System.Windows.Forms.Label NectarLbl2;
        private System.Windows.Forms.Label FramesRunLbl1;
        private System.Windows.Forms.Label FramesRunLbl2;
        private System.Windows.Forms.Label FrameRateLbl1;
        private System.Windows.Forms.Label FrameRateLbl2;
        private System.Windows.Forms.ListBox listBox;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton OpenToolStripButton;
        private System.Windows.Forms.ToolStripButton NameToolStripButton;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.ToolStripButton printToolStripButton;
        private System.Windows.Forms.ToolStripButton SettingsBtn;
    }
}

