﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace BeeWorld
{
    public partial class Form1 : Form
    {
        private HiveForm hiveForm;
        private FieldForm fieldForm;
        private SettingsForm settingsForm = new SettingsForm();
        private Renderer renderer;

        private World world;
        public BeeConfig config;
        private Random random = new Random();
        private DateTime start = DateTime.Now;  //вычисляется время работы симулятора
        private DateTime end;
        private int framesRun = 0;              //сколько кадров уже показано
        public bool SimulationStarted = false;

        public Form1()
        {
            InitializeComponent();
            config = new BeeConfig(6, 8, 3.2, 15.0);
        }
        
        private void UpdateStats(TimeSpan frameDuration)
        {
            BeesLbl2.Text = world.Bees.Count.ToString();
            FlowersLbl2.Text = world.Flowers.Count.ToString();
            HoneyLbl2.Text = String.Format("{0:f3}", world.Hive.Honey);
            double nectar = 0;
            foreach (Flower flower in world.Flowers)
                nectar += flower.Nectar;
            NectarLbl2.Text = String.Format("{0:f3}", nectar);
            FramesRunLbl2.Text = framesRun.ToString();
            double milliSeconds = frameDuration.TotalMilliseconds;
            if (milliSeconds != 0.0)
                FrameRateLbl2.Text = string.Format("{0:f0} ({1:f1}ms)", 1000 / milliSeconds, milliSeconds);
            else
                FrameRateLbl2.Text = "N/A";
        }

        private void MoveChildForms()
        {
            hiveForm.Location = new Point(this.Location.X + Width + 10, Location.Y);
            fieldForm.Location = new Point(this.Location.X, Location.Y + Math.Max(Height, hiveForm.Height) + 10);
        }

        //Прогон 1 кадра:
        public void RunFrame(object sender, EventArgs e)
        {
            framesRun++;                            //увеличивается кол-во кадров на 1
            world.Go(random);
            end = DateTime.Now;
            TimeSpan frameDuration = end - start;   //сколько времени прошло с момента проигрывания последнего кадра
            start = end;
            UpdateStats(frameDuration);             //обновили статистику
            hiveForm.Invalidate();
            fieldForm.Invalidate();
        }

        private void ResetSimulator()
        {
            fieldForm = new FieldForm();
            hiveForm = new HiveForm();
            framesRun = 0;
            world = new World(new BeeMessage(SendMessage), config);
            renderer = new Renderer(world, hiveForm, fieldForm);
            MoveChildForms();
        }

        private void StartBtn_Click(object sender, EventArgs e)
        {
            if (SimulationStarted == false) {
                ResetSimulator();
                timer1.Interval = 50;                       //запуск каждые 50 млс (20 раз в сек)
                timer1.Tick += new EventHandler(RunFrame);  //связано с методом RunFrame
                timer2.Start();                             //включает пчел =)
                UpdateStats(new TimeSpan());
                fieldForm.Show();
                hiveForm.Show();
                MoveChildForms();
                SimulationStarted = true;
                SettingsBtn.Enabled = false;
            }
            if (timer1.Enabled) {
                ToolStrip1.Items[0].Text = "Resume simulation";
                timer1.Stop();
            } else {
                
                ToolStrip1.Items[0].Text = "Pause simulation";
                timer1.Start();
            }
        }

        private void StopBtn_Click(object sender, EventArgs e)
        {
            //if (timer1.Enabled)
            //{
                ToolStrip1.Items[0].Text = "Start simulation";
                timer1.Stop();
                timer1.Tick -= new EventHandler(RunFrame);
                timer2.Stop();
                fieldForm.Close();
                hiveForm.Close();
                ResetSimulator();
                SimulationStarted = false;
                SettingsBtn.Enabled = true;
            //}
        }

        private void SendMessage(int ID, string Message)
        {
            statusStrip1.Items[0].Text = "Bee #" + ID + ": " + Message;
            //LINQ группирует пчел по св-ву CurrentState:
            var beeGroups =
                from bee in world.Bees
                group bee by bee.CurrentState into beeGroup
                orderby beeGroup.Key
                select beeGroup;
            //Выводим информацию в listbox:
            listBox.Items.Clear();
            foreach (var group in beeGroups)
            {
                string s;
                if (group.Count() == 1) s = "";
                else s = "s";
                listBox.Items.Add(group.Key.ToString() + ": "
                                + group.Count() + " bee" + s);
                //Если все пчелы не заняты, симуляция закончилась:
                if (group.Key == BeeState.Idle && group.Count() == world.Bees.Count()
                    && framesRun > 0)
                {
                    listBox.Items.Add("Simulation ended: all bees are idle!");
                    ToolStrip1.Items[0].Text = "Simulation ended";
                    statusStrip1.Items[0].Text = "Simulation ended";
                    timer1.Enabled = false;
                }
            }
        }

        private void SaveToolStripButton_Click(object sender, EventArgs e)
        {
            bool enabled = timer1.Enabled;
            if (enabled) timer1.Stop();

            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.Filter = "Simulator File (*.bees)|*.bees";
            saveDialog.CheckPathExists = true;
            saveDialog.Title = "Choose a file to save the current simulation";
            if (saveDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    BinaryFormatter bf = new BinaryFormatter();
                    using (Stream output = File.OpenWrite(saveDialog.FileName))
                    {
                        //Сохраняется весь мир (т.к. world содержит ссылки на другие объекты):
                        bf.Serialize(output, world);
                        bf.Serialize(output, framesRun);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Unable to save simulator file\r\n" + ex.Message,
                        "Bee Simulator Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            if (enabled) timer1.Start();    //после сохранения файла можно запутить таймер
        }

        private void OpenToolStripButton_Click(object sender, EventArgs e)
        {
            //резервная копия текущего состояния мира для отката при ошибках:
            World currentWorld = world;
            int currentFramesRun = framesRun;

            bool enabled = timer1.Enabled;
            if (enabled) timer1.Stop();

            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = "Simulator File (*.bees)|*.bees";
            openDialog.CheckPathExists = true;
            openDialog.CheckFileExists = true;
            openDialog.Title = "Выберите файл симулятора пчел";
            if (openDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    BinaryFormatter bf = new BinaryFormatter();
                    using (Stream input = File.OpenRead(openDialog.FileName))
                    {
                        world = (World)bf.Deserialize(input);
                        framesRun = (int)bf.Deserialize(input);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Невозможно прочитать файл симулятора\r\n" + ex.Message,
                        "Bee Simulator Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    world = currentWorld;           //если ошибка - грузим последнее состояние мира
                    framesRun = currentFramesRun;
                }
            }
            //После загрузки файла подсоединяем делегат:
            world.Hive.MessageSender = new BeeMessage(SendMessage);
            foreach (Bee bee in world.Bees)
                bee.MessageSender = new BeeMessage(SendMessage);
            if (enabled)
                timer1.Start();     //перезапускаем таймер
            renderer = new Renderer(world, hiveForm, fieldForm);
        }

        private void Form1_Move(object sender, EventArgs e)
        {
            if (SimulationStarted == true)
                MoveChildForms();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            renderer.AnimateBees();
        }

#region printing
        private void printToolStripButton_Click(object sender, EventArgs e)
        {
            bool stoppedTimer = false;
            if (timer1.Enabled)
            {
                timer1.Stop();
                stoppedTimer = true;
            }
            PrintPreviewDialog preview = new PrintPreviewDialog();
            PrintDocument document = new PrintDocument();
            preview.Document = document;
            document.PrintPage += new PrintPageEventHandler(document_PrintPage);
            preview.ShowDialog(this);
            if (stoppedTimer) timer1.Start();
        }

        void document_PrintPage(object sender, PrintPageEventArgs e)
        {
            Graphics g = e.Graphics;

            Size stringSize;
            //Рисуем эллипс с надписью:
            using (Font arial24bold = new Font("Arial", 24, FontStyle.Bold))
            {
                stringSize = Size.Ceiling(g.MeasureString("Bee Simulator", arial24bold));
                g.FillEllipse(Brushes.Green, new Rectangle(e.MarginBounds.X+2, e.MarginBounds.Y+2,
                    stringSize.Width + 30, stringSize.Height + 30));
                g.FillEllipse(Brushes.DarkGreen, new Rectangle(e.MarginBounds.X, e.MarginBounds.Y,
                    stringSize.Width + 30, stringSize.Height + 30));
                g.DrawString("Bee Simulator", arial24bold, Brushes.Yellow, e.MarginBounds.X + 17, e.MarginBounds.Y + 17);
                g.DrawString("Bee Simulator", arial24bold, Brushes.White, e.MarginBounds.X + 15, e.MarginBounds.Y + 15);
            }
            int tableX = e.MarginBounds.X + (int)stringSize.Width + 50;
            int tableWidth = e.MarginBounds.X + e.MarginBounds.Width - tableX - 20;
            int firstColumnX = tableX + 2;
            int secondColumnX = tableX + (tableWidth / 2) + 5;
            int tableY = e.MarginBounds.Y;
            //Рисуем таблицу:
            tableY = PrintTableRow(g, tableX, tableWidth, firstColumnX, secondColumnX, tableY, "Bees", BeesLbl2.Text);
            tableY = PrintTableRow(g, tableX, tableWidth, firstColumnX, secondColumnX, tableY, "Flowers", FlowersLbl2.Text);
            tableY = PrintTableRow(g, tableX, tableWidth, firstColumnX, secondColumnX, tableY, "Honey in Hive", HoneyLbl2.Text);
            tableY = PrintTableRow(g, tableX, tableWidth, firstColumnX, secondColumnX, tableY, "Nectar in flowers", NectarLbl2.Text);
            tableY = PrintTableRow(g, tableX, tableWidth, firstColumnX, secondColumnX, tableY, "Frames run", FramesRunLbl2.Text);
            tableY = PrintTableRow(g, tableX, tableWidth, firstColumnX, secondColumnX, tableY, "Frame Rate", FrameRateLbl2.Text);
            g.DrawRectangle(Pens.Black, tableX, e.MarginBounds.Y, tableWidth, tableY - e.MarginBounds.Y);
            g.DrawLine(Pens.Black, secondColumnX, e.MarginBounds.Y, secondColumnX, tableY);
            //Рисуем снимки форм:
            using (Pen blackPen = new Pen(Brushes.Black, 2))
            using (Bitmap hiveBitmap = new Bitmap (hiveForm.ClientSize.Width, hiveForm.ClientSize.Height))
            using (Bitmap fieldBitmap = new Bitmap(fieldForm.ClientSize.Width, fieldForm.ClientSize.Height))
            { 
                using (Graphics hiveGraphics = Graphics.FromImage(hiveBitmap))
                {
                    renderer.PaintHive(hiveGraphics);
                }
                int hiveWidth = e.MarginBounds.Width / 2;
                float ratio = (float)hiveBitmap.Height / (float)hiveBitmap.Width;
                int hiveHeight = (int)(hiveWidth * ratio);
                int hiveX = e.MarginBounds.X + (e.MarginBounds.Width - hiveWidth) / 2;
                int hiveY = e.MarginBounds.Height / 3;
                g.DrawImage(hiveBitmap, hiveX, hiveY, hiveWidth, hiveHeight);
                g.DrawRectangle(blackPen, hiveX, hiveY, hiveWidth, hiveHeight);

                using (Graphics fieldGraphics = Graphics.FromImage(fieldBitmap))
                {
                    renderer.PaintField(fieldGraphics);
                }
                int fieldWidth = e.MarginBounds.Width;
                ratio = (float)fieldBitmap.Height / (float)fieldBitmap.Width;
                int fieldHeight = (int)(fieldWidth * ratio);
                int fieldX = e.MarginBounds.X;
                int fieldY = e.MarginBounds.Y + e.MarginBounds.Height - fieldHeight;
                g.DrawImage(fieldBitmap, fieldX, fieldY, fieldWidth, fieldHeight);
                g.DrawRectangle(blackPen, fieldX, fieldY, fieldWidth, fieldHeight);

            }
        }

        private int PrintTableRow(Graphics printGraphics, int tableX, int tableWidth, int firstColumnX, int secondColumnX, int tableY, string firstColumn, string secondColumn)
        {
            Font arial12 = new Font("Arial", 12);
            Size stringSize = Size.Ceiling(printGraphics.MeasureString(firstColumn, arial12));
            tableY += 2;
            printGraphics.DrawString(firstColumn, arial12, Brushes.Black, firstColumnX, tableY);
            printGraphics.DrawString(secondColumn, arial12, Brushes.Black, secondColumnX, tableY);
            tableY += (int)stringSize.Height + 2;
            printGraphics.DrawLine(Pens.Black, tableX, tableY, tableX + tableWidth, tableY);
            arial12.Dispose();
            return tableY;
        }
#endregion //< printing

        private void SettingsBtn_Click(object sender, EventArgs e)
        {
            settingsForm.UpdateStats(config);
            settingsForm.ShowDialog();
            settingsForm.Left = this.Left;
            settingsForm.Top = this.Top;
        }

    }
}
