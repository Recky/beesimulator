﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace BeeWorld
{
    [Serializable]
    public class Hive
    {
        public int InitialBees;
        public int MaximumBees;
        public double InitialHoney;
        public double MaximumHoney;
        private const double NectarHoneyRatio = 0.25;
        private const double MinimalHoneyForCreatingBees = 4.0;


        private Dictionary<string, Point> locations;
        private int beeCount = 0;
        public double Honey { get; private set; }

        private World world;

        private void InitializeLocations()
        {
            locations = new Dictionary<string, Point>();
            locations.Add("Entrance", new Point(612, 101));
            locations.Add("Nursery", new Point(100, 187));
            locations.Add("HoneyFactory", new Point(189, 94));
            locations.Add("Exit", new Point(213, 231));
        }

        public Point GetLocation(string location)
        {
            if (locations.Keys.Contains(location))
                return locations[location];
            else
                throw new ArgumentException("Unknown Location: " + location);
        }

        [NonSerialized]
        public BeeMessage MessageSender;

        public Hive(World world, BeeMessage MessageSender)
        {
            InitialBees = world.Config.InitialBees;
            MaximumBees = world.Config.MaximumBees;
            InitialHoney = world.Config.InitialHoney;
            MaximumHoney = world.Config.MaximumHoney;
            this.MessageSender = MessageSender;
            this.world = world;
            Honey = InitialHoney;
            InitializeLocations();
            Random random = new Random();
            for (int i = 0; i < InitialBees; i++)
                AddBee(random);
        }

        public bool AddHoney(double nectar) {
            double honeyToAdd = nectar * NectarHoneyRatio;
            if (honeyToAdd + Honey > MaximumHoney)
                return false;
            Honey += honeyToAdd;
            return true; 
        }
        public bool ConsumeHoney(double amount) {
            if (amount > Honey)
                return false;
            else {
                Honey -= amount;
                return true; 
            }
        }
        public void AddBee(Random random) {
            beeCount++;
            int r1 = random.Next(100) - 50;
            int r2 = random.Next(100) - 50;
            Point startPoint = new Point(locations["Nursery"].X + r1, locations["Nursery"].Y + r2);
            Bee newBee = new Bee(beeCount, startPoint, world, this);
            newBee.MessageSender += this.MessageSender;             //каждая пчела получает метод на который указывает делегат
            world.Bees.Add(newBee);
        }
        public void Go(Random random) {
            if (world.Bees.Count < MaximumBees && Honey > MinimalHoneyForCreatingBees && random.Next(10) == 1)
                AddBee(random);
        }
    }
}
