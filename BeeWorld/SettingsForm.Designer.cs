﻿namespace BeeWorld
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.MaximumHoneyBox = new System.Windows.Forms.TextBox();
            this.InitialHoneyBox = new System.Windows.Forms.TextBox();
            this.MaximumBeesBox = new System.Windows.Forms.TextBox();
            this.InitialBeesBox = new System.Windows.Forms.TextBox();
            this.InitialBeesLbl1 = new System.Windows.Forms.Label();
            this.InitialHoneyLbl1 = new System.Windows.Forms.Label();
            this.MaximumBeesLbl1 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ApplyBtn = new System.Windows.Forms.Button();
            this.CancelBtn = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.MaximumHoneyBox, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.InitialHoneyBox, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.MaximumBeesBox, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.InitialBeesBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.InitialBeesLbl1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.InitialHoneyLbl1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.MaximumBeesLbl1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(320, 78);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // MaximumHoneyBox
            // 
            this.MaximumHoneyBox.Location = new System.Drawing.Point(163, 61);
            this.MaximumHoneyBox.Name = "MaximumHoneyBox";
            this.MaximumHoneyBox.Size = new System.Drawing.Size(100, 20);
            this.MaximumHoneyBox.TabIndex = 10;
            // 
            // InitialHoneyBox
            // 
            this.InitialHoneyBox.Location = new System.Drawing.Point(163, 41);
            this.InitialHoneyBox.Name = "InitialHoneyBox";
            this.InitialHoneyBox.Size = new System.Drawing.Size(100, 20);
            this.InitialHoneyBox.TabIndex = 9;
            // 
            // MaximumBeesBox
            // 
            this.MaximumBeesBox.Location = new System.Drawing.Point(163, 22);
            this.MaximumBeesBox.Name = "MaximumBeesBox";
            this.MaximumBeesBox.Size = new System.Drawing.Size(100, 20);
            this.MaximumBeesBox.TabIndex = 8;
            // 
            // InitialBeesBox
            // 
            this.InitialBeesBox.Location = new System.Drawing.Point(163, 3);
            this.InitialBeesBox.Name = "InitialBeesBox";
            this.InitialBeesBox.Size = new System.Drawing.Size(100, 20);
            this.InitialBeesBox.TabIndex = 1;
            // 
            // InitialBeesLbl1
            // 
            this.InitialBeesLbl1.AutoSize = true;
            this.InitialBeesLbl1.Location = new System.Drawing.Point(3, 0);
            this.InitialBeesLbl1.Name = "InitialBeesLbl1";
            this.InitialBeesLbl1.Size = new System.Drawing.Size(149, 13);
            this.InitialBeesLbl1.TabIndex = 1;
            this.InitialBeesLbl1.Text = "Начальное количество пчёл";
            this.InitialBeesLbl1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // InitialHoneyLbl1
            // 
            this.InitialHoneyLbl1.AutoSize = true;
            this.InitialHoneyLbl1.Location = new System.Drawing.Point(3, 38);
            this.InitialHoneyLbl1.Name = "InitialHoneyLbl1";
            this.InitialHoneyLbl1.Size = new System.Drawing.Size(152, 13);
            this.InitialHoneyLbl1.TabIndex = 2;
            this.InitialHoneyLbl1.Text = "Начальное количество мёда";
            this.InitialHoneyLbl1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // MaximumBeesLbl1
            // 
            this.MaximumBeesLbl1.AutoSize = true;
            this.MaximumBeesLbl1.Location = new System.Drawing.Point(3, 19);
            this.MaximumBeesLbl1.Name = "MaximumBeesLbl1";
            this.MaximumBeesLbl1.Size = new System.Drawing.Size(87, 13);
            this.MaximumBeesLbl1.TabIndex = 4;
            this.MaximumBeesLbl1.Text = "Максимум пчёл";
            this.MaximumBeesLbl1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Максимум мёда";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // ApplyBtn
            // 
            this.ApplyBtn.Location = new System.Drawing.Point(49, 84);
            this.ApplyBtn.Name = "ApplyBtn";
            this.ApplyBtn.Size = new System.Drawing.Size(103, 21);
            this.ApplyBtn.TabIndex = 1;
            this.ApplyBtn.Text = "Применить";
            this.ApplyBtn.UseVisualStyleBackColor = true;
            this.ApplyBtn.Click += new System.EventHandler(this.ApplyBtn_Click);
            // 
            // CancelBtn
            // 
            this.CancelBtn.Location = new System.Drawing.Point(163, 84);
            this.CancelBtn.Name = "CancelBtn";
            this.CancelBtn.Size = new System.Drawing.Size(103, 21);
            this.CancelBtn.TabIndex = 2;
            this.CancelBtn.Text = "Отмена";
            this.CancelBtn.UseVisualStyleBackColor = true;
            this.CancelBtn.Click += new System.EventHandler(this.CancelBtn_Click);
            // 
            // SettingsForm
            // 
            this.AcceptButton = this.ApplyBtn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(320, 109);
            this.Controls.Add(this.CancelBtn);
            this.Controls.Add(this.ApplyBtn);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "SettingsForm";
            this.Text = "Settings";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label InitialBeesLbl1;
        private System.Windows.Forms.Label InitialHoneyLbl1;
        private System.Windows.Forms.Label MaximumBeesLbl1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox MaximumHoneyBox;
        private System.Windows.Forms.TextBox InitialHoneyBox;
        private System.Windows.Forms.TextBox MaximumBeesBox;
        private System.Windows.Forms.TextBox InitialBeesBox;
        private System.Windows.Forms.Button ApplyBtn;
        private System.Windows.Forms.Button CancelBtn;
    }
}