﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BeeWorld
{
    public partial class SettingsForm : Form
    {
        public SettingsForm()
        {
            InitializeComponent();
        }

        private BeeConfig config;

        public void UpdateStats(BeeConfig config)
        {
            this.config = config;
            this.InitialBeesBox.Text = config.InitialBees.ToString();
            this.MaximumBeesBox.Text = config.MaximumBees.ToString();
            this.InitialHoneyBox.Text = config.InitialHoney.ToString();
            this.MaximumHoneyBox.Text = config.MaximumHoney.ToString();
        }

        private void ApplyConfigChanges()
        {
            config.InitialBees = Int32.Parse(this.InitialBeesBox.Text);
            config.MaximumBees = Int32.Parse(this.MaximumBeesBox.Text);
            config.InitialHoney = double.Parse(this.InitialHoneyBox.Text);
            config.MaximumHoney = double.Parse(this.MaximumHoneyBox.Text);
        }

        private void ApplyBtn_Click(object sender, EventArgs e)
        {
            ApplyConfigChanges();
            this.Close();
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
